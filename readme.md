# Autonomous car driving

This repository includes:

* **get_data_format.py** - script for processing the data
* **X.npy Y.npy** - proccesed data in binary format
* **mymodel.h5** - working neural network trained using Google Colab
* **car_control** - script for managing controls of the car simulator in real time

In this project I used my own simulator recordings on the first of the possible maps.
Here is some recording I took when the **neural network is in charge of controlling the car**:

![](gif1.gif)  

As you can see it's working on small velocities up to 20 mps but it swerves a lot above it.  
I also checked how it behaves on the other map and it did quite well.  

![](gif2.gif)  

Although the neural network saw that map for the first time, it was good for generalizing the data.  
There's still some room for major improvements and it's very interesting project so I'll **definitely** be revisiting this in the future.
