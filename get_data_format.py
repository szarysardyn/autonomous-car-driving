#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 15 15:29:32 2019

@author: szary
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image



df = pd.read_csv('/home/szary/Python NN/self driving car with my data/data/driving_log.csv', 
                 names = ['center', 'left', 'right', 'steering angle', 'throttle', 'break', 'speed'])

def load_image(fname):
    return np.asarray(Image.open(fname))

def process_image(img):
    return img[10:130:2, ::4, :]

X = [process_image(load_image(fname)) for fname in df['center']]
X += [process_image(load_image(fname)) for fname in df['left']]
X += [process_image(load_image(fname)) for fname in df['right']]
X = np.array(X)

Y = np.array(df['steering angle'])
Y = np.concatenate([Y, Y + 0.4, Y - 0.4])

print(X.shape)
print(Y.shape)

plt.plot(Y)
plt.show()

np.save('X.npy', X)
np.save('Y.npy', Y)