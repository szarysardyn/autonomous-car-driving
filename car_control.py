#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 14 16:10:34 2019

@author: szary
"""

from flask import Flask
import eventlet.wsgi
import eventlet
import socketio
import base64
import numpy as np
from PIL import Image
from io import BytesIO
from keras.models import load_model

sio = socketio.Server()
app = Flask(__name__)



def send_control(steering_angle, throttle):
    sio.emit("steer", data={'steering_angle': str(steering_angle),
                            'throttle': str(throttle) }, skip_sid=True)

def process_image(image):
    return image[10:130:2, ::4, :]
    
model = load_model('/home/szary/Python NN/self driving car with my data/mymodel.h5')
model.summary()
    
@sio.on('telemetry')
def telemetry(sid, data):
    if data:
        speed = float(data["speed"])
        image_str = data["image"]
        
        decoded = base64.b64decode(image_str)
        image = Image.open(BytesIO(decoded))
        image_array = np.asarray(image)
        
        img = process_image(image_array)
        #type(image)
        img_batch = np.expand_dims(img, axis = 0)
        
        steering_angle = float(model.predict(img_batch))
        throttle = 0.15
        
        if speed < 16:
            throttle = 0.5
        elif speed > 18:
            throttle = -0.1

        send_control(steering_angle, throttle)
    else:
        sio.emit('manual', data={}, skip_sid=True)
        
app = socketio.Middleware(sio, app)
eventlet.wsgi.server(eventlet.listen(('', 4567)), app)